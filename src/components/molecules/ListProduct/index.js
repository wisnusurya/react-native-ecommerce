import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { ILShopeeMallGreatSale, ILHospitalBG } from '../../../assets'
import { colors } from '../../../utils'

const ListProduct = ({ image, nama_produk, harga }) => {
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Image source={{ uri: image }} style={styles.picture} />
                <Text style={styles.productName}>{nama_produk}</Text>
                <Text style={styles.price}>Rp{harga}</Text>
            </View>
        </View>
    )
}

export default ListProduct

const styles = StyleSheet.create({
    container: {
        borderRadius: 10,
        margin: 5,
    },
    content: {
        borderRadius: 10,
        backgroundColor: colors.white,
        elevation: 2,
    },
    picture: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        width: 150,
        height: 200
    },
    productName: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        maxWidth: 150,
    },
    price: {
        paddingHorizontal: 10,
        marginBottom: 10,
        maxWidth: 150,
    },
})
