import {combineReducers} from 'redux';
import dummy from './dummy';
import getCart from './getCart';
import login from './login';

const rootReducer = combineReducers({
  dummy,
  getCart,
  login,
});
export default rootReducer;
