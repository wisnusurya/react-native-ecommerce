import * as types from '../action/login';
const initialState = {
  respLoginOk: null,
  respLoginFail: null,
};
const login = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_OK:
       console.log('Reducer OK: ', action.data);
      return {
        ...state,
        respLoginOk: action.data,
      };
    case types.LOGIN_FAIL:
       console.log('Reducer FAIL: ', action.data);
      return {
        ...state,
        respLoginFail: action.data,
      };
    default:
      return state;
  }
};
export default login;
