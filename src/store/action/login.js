import {login} from '../../api';
export const LOGIN_OK = 'LOGIN_OK';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const requestLogin = (email, password) => dispatch => {

  const payload = {
    email,
    password,
  };

  login(payload).then(({data}) => {
      // dispatch(respRegOk(data));
      console.log('loginData ', data);
    })
    .catch(e => {
      dispatch(respRegFail(e));
      console.log('loginData ', e);
    });
};
const respRegOk = data => {
  return {
    type: LOGIN_OK,
    data,
  };
};
const respRegFail = data => {
  return {
    type: LOGIN_FAIL,
    data,
  };
};
